{
    description = "Simple URL shortener";
    inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    outputs = inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
        systems = [
            "x86_64-linux" "x86_64-darwin"
            "aarch64-linux" "aarch64-darwin"
        ];

        perSystem = { config, self', inputs', pkgs, system, ... }: {
            devShells.default = with pkgs; mkShell {
                buildInputs = ([
                    nodejs_20
                    ferretdb
                    postgresql
                ]) ++ (with nodePackages; [ pnpm ]);

                shellHook = ''
                export PGHOST=$PWD/.postgres
                export PGDATA=$PGHOST/data
                export PGDATABASE=postgres
                export PGLOG=$PGHOST/postgres.log
                export LANG="en_US.UTF-8"

                mkdir -p $PGHOST

                if [ ! -d $PGDATA ]
                then
                initdb -U $USER -A trust > /dev/null
                fi

                if ! pg_ctl status -s > /dev/null
                then
                pg_ctl start -l $PGLOG -o --unix_socket_directories='$PGHOST' > /dev/null
                fi

                if ! psql -U $USER --list | grep -q ferretdb
                then
                createdb -U $USER ferretdb
                fi

                ferretdb > /dev/null 2>&1 &
                trap "pg_ctl stop > /dev/null && pkill ferretdb" EXIT
                '';
            };
        };
    };
}

